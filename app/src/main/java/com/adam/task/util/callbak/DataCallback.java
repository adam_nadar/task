package com.adam.task.util.callbak;

//comment by sunny jain
public interface DataCallback<R, E> {
    void onSuccess(R response);

    void onError(E error);
}

