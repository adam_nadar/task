package com.adam.task.util.network;

import com.adam.task.entity.Users;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TaskService {
    @GET("users")
    Call<List<Users>> getUsers();
}
