package com.adam.task.UserCordinates;

public class UserCordinateContract {
    public interface View {
        void endActivity();
    }

    public interface Presenter {
        void onHomeMenu();
    }

}
