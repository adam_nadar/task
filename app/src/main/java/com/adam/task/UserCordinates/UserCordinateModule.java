package com.adam.task.UserCordinates;

import dagger.Module;
import dagger.Provides;

@Module
public class UserCordinateModule {
    @Provides
    UserCordinateContract.View provideUserCordinteActView(UserCordinteAct userCordinteAct) {
        return userCordinteAct;
    }

    @Provides
    UserCordinateContract.Presenter providesUserCordinteActPresenter(UserCordinateContract.View view) {
        return new UserCordinatePresenter(view);
    }
}
