package com.adam.task.UserCordinates;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.adam.task.R;
import com.adam.task.databinding.ActivityUserCordinteBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class UserCordinteAct extends AppCompatActivity implements OnMapReadyCallback,
        UserCordinateContract.View {

    @Inject
    UserCordinateContract.Presenter presenter;
    private ActivityUserCordinteBinding binding;
    private Context context;
    private double lat = -1;
    private double lang = -1;
    private String content = "";
    private LatLng add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_cordinte);
        context = this;

        setSupportActionBar(binding.toolbar);
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            bar.setTitle(getString(R.string.address_location_title));
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            lat = bundle.getDouble("lat", -1);
            lang = bundle.getDouble("lang", -1);
            content = bundle.getString("content", "");
            add = new LatLng(lat, lang);
        }
        binding.contentPanel.setText(content);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.userMap);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (add != null) {
                googleMap.addMarker(new MarkerOptions().position(add).title("title").snippet("snippet"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(add));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home:
                presenter.onHomeMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void endActivity() {
        setResult(RESULT_OK);
        finish();
    }
}
