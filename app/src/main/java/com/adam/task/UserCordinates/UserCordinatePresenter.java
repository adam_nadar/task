package com.adam.task.UserCordinates;

import javax.inject.Inject;

public class UserCordinatePresenter implements UserCordinateContract.Presenter {

    private UserCordinateContract.View view;

    @Inject
    public UserCordinatePresenter(UserCordinateContract.View view) {
        this.view = view;
    }

    @Override
    public void onHomeMenu() {
        view.endActivity();
    }
}
