package com.adam.task.ListHome.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.R;
import com.adam.task.databinding.ActivityListBinding;
import com.adam.task.entity.Users;
import com.adam.task.userDetail.view.UserDetailActivity;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class ListAct extends AppCompatActivity implements ListActContract.View {

    @Inject
    ListActContract.Presenter presenter;
    @Inject
    UserListAdapter usersAdapter;
    private ActivityListBinding binding;
    private Context context;

    public static void start(Context context) {
        context.startActivity(new Intent(context, ListAct.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);
        setTitle(R.string.List_title);

        context = this;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        binding.rvUsers.setLayoutManager(gridLayoutManager);
        binding.rvUsers.setAdapter(usersAdapter);
        binding.showUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.viewDetailsClicked();
            }
        });
        usersAdapter.setListener(new UserListAdapter.ClickListener() {
            @Override
            public void onClickListener(int position, boolean b) {
                presenter.onUsersClicked(position, b);
            }
        });

        binding.btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getUsers();
            }
        });

        binding.btnTryAgain.performClick();
    }

    @Override
    public void setUsers(List<Users> users) {
        usersAdapter.setData(users);
    }

    @Override
    public void UsersView(boolean show) {
        binding.rvUsers.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void loadingView(boolean isLoading) {
        binding.pbLoading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void errorView(boolean show) {
        binding.llErrorView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showButton(boolean show) {
        binding.showUserDetails.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDetailsView() {
        UserDetailActivity.start(context);
    }
}
