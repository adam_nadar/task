package com.adam.task.ListHome.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adam.task.R;
import com.adam.task.databinding.UserListBinding;
import com.adam.task.entity.Users;

import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {

    private List<Users> data;
    private ClickListener listener;

    public UserListAdapter() {
        data = new ArrayList<>();
    }

    public void setData(List<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(ClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        UserListBinding vie = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.user_list, viewGroup, false);
        return new UserViewHolder(vie);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int i) {
        Users user = data.get(i);
        userViewHolder.binding.name.setText(userViewHolder.itemView.getContext()
                .getString(R.string.user_list_act_name, user.getName()));
        userViewHolder.binding.mail.setText(userViewHolder.itemView.getContext()
                .getString(R.string.user_list_act_mail, user.getEmail()));
        userViewHolder.binding.select.setImageResource(
                user.isSelected() ?
                        R.drawable.ic_checkmark_flat :
                        R.drawable.unchecked_checkbox_grey
        );
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    interface ClickListener {
        void onClickListener(int position, boolean b);
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        private UserListBinding binding;

        UserViewHolder(UserListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int pos = getAdapterPosition();
                        Users u = data.get(pos);
                        u.setSelected(!u.isSelected());
                        listener.onClickListener(pos, u.isSelected());
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }
}
