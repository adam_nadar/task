package com.adam.task.ListHome.presenter;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.entity.Users;
import com.adam.task.util.callbak.DataCallback;
import com.adam.task.util.callbak.ToggleButtonViability;

import java.util.List;

import javax.inject.Inject;

public class ListActPresenter implements ListActContract.Presenter {

    private ListActContract.View view;
    private ListActContract.Model model;

    @Inject
    public ListActPresenter(ListActContract.View view, ListActContract.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void getUsers() {
        view.loadingView(true);
        view.UsersView(false);
        view.errorView(false);
        model.fetchUsers(new DataCallback<List<Users>, Throwable>() {
            @Override
            public void onSuccess(List<Users> response) {
                view.setUsers(response);
                view.UsersView(true);
                view.errorView(false);
                view.loadingView(false);
            }

            @Override
            public void onError(Throwable error) {
                view.errorView(true);
                view.UsersView(false);
                view.loadingView(false);
            }
        });
    }

    @Override
    public void onUsersClicked(int position, boolean isChecked) {
        model.UpdateUserStatus(position, isChecked, new ToggleButtonViability() {
            @Override
            public void toggle(boolean visibility) {
                view.showButton(visibility);
            }
        });
    }

    @Override
    public void viewDetailsClicked() {
        view.showDetailsView();
    }
}
