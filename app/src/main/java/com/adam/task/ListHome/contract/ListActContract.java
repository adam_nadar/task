package com.adam.task.ListHome.contract;

import com.adam.task.entity.Users;
import com.adam.task.util.callbak.DataCallback;
import com.adam.task.util.callbak.ToggleButtonViability;

import java.util.List;

public class ListActContract {
    public interface Model {
        void fetchUsers(DataCallback<List<Users>, Throwable> dataCallback);

        Users getUsersAtPosition(int position);

        void UpdateUserStatus(int position, boolean isSelected, ToggleButtonViability visibilityCallback);
    }

    public interface View {
        void setUsers(List<Users> users);

        void UsersView(boolean show);

        void loadingView(boolean isLoading);

        void errorView(boolean show);

        void showButton(boolean show);

        void showDetailsView();
    }

    public interface Presenter {
        void getUsers();

        void onUsersClicked(int position, boolean isChecked);

        void viewDetailsClicked();
    }
}