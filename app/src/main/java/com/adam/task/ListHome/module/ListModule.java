package com.adam.task.ListHome.module;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.ListHome.presenter.ListActPresenter;
import com.adam.task.ListHome.view.ListAct;
import com.adam.task.ListHome.view.UserListAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListModule {

    @Provides
    ListActContract.View provideListActView(ListAct listAct) {
        return listAct;
    }

    @Provides
    ListActContract.Presenter providesListActPresenter(ListActContract.View view,
                                                       ListActContract.Model model) {
        return new ListActPresenter(view, model);
    }

    @Provides
    UserListAdapter provideUserListAdapter() {
        return new UserListAdapter();
    }
}
