package com.adam.task.ListHome.model;

import android.support.annotation.NonNull;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.entity.Users;
import com.adam.task.util.callbak.DataCallback;
import com.adam.task.util.callbak.ToggleButtonViability;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActModel implements ListActContract.Model {

    private List<Users> userList = null;
    private Call<List<Users>> call;

    @Inject
    public ListActModel(Call<List<Users>> call) {
        this.call = call;
    }

    @Override
    public void fetchUsers(final DataCallback<List<Users>, Throwable> dataCallback) {
        if (userList != null)
            dataCallback.onSuccess(userList);
        else {
            if (!call.isExecuted()) {
                call.enqueue(new Callback<List<Users>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<Users>> call, @NonNull Response<List<Users>> response) {
                        userList = response.body();
                        if (response.isSuccessful() && userList != null) {
                            dataCallback.onSuccess(userList);
                        } else {
                            dataCallback.onError(new Throwable("Error"));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Users>> call, @NonNull Throwable t) {
                        dataCallback.onError(new Throwable("Error"));
                    }
                });
            }
        }
    }

    @Override
    public Users getUsersAtPosition(int position) {
        return userList == null ? null : userList.get(position);
    }

    @Override
    public void UpdateUserStatus(int position, boolean isSelected, ToggleButtonViability callback) {
        boolean show = false;
        if (userList != null) {
            userList.get(position).setSelected(isSelected);
            for (Users user : userList)
                if (user.isSelected()) {
                    show = true;
                    break;
                }
            callback.toggle(show);
        } else
            callback.toggle(show);
    }
}
