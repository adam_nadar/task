package com.adam.task.userDetail.presenter;

import com.adam.task.entity.Users;
import com.adam.task.userDetail.contract.UserDetailContract;
import com.adam.task.util.callbak.DataCallback;

import java.util.List;

import javax.inject.Inject;

public class UserDetailPresenter implements UserDetailContract.Presenter {

    private UserDetailContract.View view;
    private UserDetailContract.Model model;

    @Inject
    public UserDetailPresenter(UserDetailContract.View view, UserDetailContract.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void getUsers() {
        view.loadingView(true);
        view.UsersView(false);
        model.fetchUsers(new DataCallback<List<Users>, Throwable>() {
            @Override
            public void onSuccess(List<Users> response) {
                view.setUsers(response);
                view.loadingView(false);
                view.UsersView(true);
            }

            @Override
            public void onError(Throwable error) {
                view.endActivity();
            }
        });
    }

    @Override
    public void onUsersClicked(int position) {
        Users user = model.getUsersAtPosition(position);
        view.showMap(user);
    }
}
