package com.adam.task.userDetail.module;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.userDetail.contract.UserDetailContract;
import com.adam.task.userDetail.model.UserDetailModel;
import com.adam.task.userDetail.presenter.UserDetailPresenter;
import com.adam.task.userDetail.view.SelectedUserAdapter;
import com.adam.task.userDetail.view.UserDetailActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class UserDetailModule {
    @Provides
    UserDetailContract.View provideUserDetailActivityView(UserDetailActivity detailActivity) {
        return detailActivity;
    }

    @Provides
    UserDetailContract.Presenter providesUserDetailActivityPresenter(UserDetailContract.View view,
                                                                     UserDetailContract.Model model) {
        return new UserDetailPresenter(view, model);
    }

    @Provides
    UserDetailContract.Model providesUserDetailModel(ListActContract.Model listModel) {
        return new UserDetailModel(listModel);
    }

    @Provides
    SelectedUserAdapter provideSelectedUserAdapter() {
        return new SelectedUserAdapter();
    }
}
