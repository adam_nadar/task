package com.adam.task.userDetail.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.adam.task.R;
import com.adam.task.UserCordinates.UserCordinteAct;
import com.adam.task.databinding.ActivityUserDetailBinding;
import com.adam.task.entity.Address;
import com.adam.task.entity.Users;
import com.adam.task.userDetail.contract.UserDetailContract;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class UserDetailActivity extends AppCompatActivity implements UserDetailContract.View {

    @Inject
    UserDetailContract.Presenter presenter;
    @Inject
    SelectedUserAdapter adapter;
    private ActivityUserDetailBinding binding;
    private Context context;

    public static void start(Context context) {
        context.startActivity(new Intent(context, UserDetailActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail);
        setTitle(getString(R.string.user_detail_act_title));
        context = this;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        binding.rcCustomerDet.setLayoutManager(linearLayoutManager);
        binding.rcCustomerDet.setAdapter(adapter);
        adapter.setListener(new SelectedUserAdapter.OnClickListener() {
            @Override
            public void onClickUser(int position) {
                presenter.onUsersClicked(position);
            }
        });
        presenter.getUsers();
    }

    @Override
    public void setUsers(List<Users> users) {
        adapter.setData(users);
    }

    @Override
    public void UsersView(boolean show) {
        binding.rcCustomerDet.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void loadingView(boolean isLoading) {
        binding.pbLoading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMap(Users user) {
        Address add = user.getAddress();
        Bundle bundle = new Bundle(3);
        bundle.putDouble("lat", add.getGeo().getLat());
        bundle.putDouble("lang", add.getGeo().getLng());
        bundle.putString("content", getString(R.string.address_lbl,
                add.getSuite(), add.getStreet(), add.getCity(), add.getZipcode()));
        Intent intent = new Intent(context, UserCordinteAct.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, 99);
    }

    @Override
    public void endActivity() {
        Toast.makeText(context, "oop's sumething went wrong", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == RESULT_OK) {
            finish();
        }
    }
}
