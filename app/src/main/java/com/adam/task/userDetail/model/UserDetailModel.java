package com.adam.task.userDetail.model;

import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.entity.Users;
import com.adam.task.userDetail.contract.UserDetailContract;
import com.adam.task.util.callbak.DataCallback;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class UserDetailModel implements UserDetailContract.Model {


    private ListActContract.Model listModel;
    private List<Users> selectedUsers;

    @Inject
    public UserDetailModel(ListActContract.Model listModel) {
        this.listModel = listModel;
    }

    @Override
    public void fetchUsers(final DataCallback<List<Users>, Throwable> dataCallback) {
        listModel.fetchUsers(new DataCallback<List<Users>, Throwable>() {
            @Override
            public void onSuccess(List<Users> response) {
                selectedUsers = new ArrayList<>();
                for (Users user : response) {
                    if (user.isSelected())
                        selectedUsers.add(user);
                }
                dataCallback.onSuccess(selectedUsers);
            }

            @Override
            public void onError(Throwable error) {
                dataCallback.onError(error);
            }
        });
    }

    @Override
    public Users getUsersAtPosition(int position) {
        return selectedUsers.get(position);
    }
}
