package com.adam.task.userDetail.contract;

import com.adam.task.entity.Users;
import com.adam.task.util.callbak.DataCallback;

import java.util.List;

public class UserDetailContract {

    public interface Model {
        void fetchUsers(DataCallback<List<Users>, Throwable> dataCallback);

        Users getUsersAtPosition(int position);

    }

    public interface View {
        void setUsers(List<Users> users);

        void UsersView(boolean show);

        void loadingView(boolean isLoading);

        void showMap(Users user);

        void endActivity();
    }

    public interface Presenter {
        void getUsers();

        void onUsersClicked(int position);
    }
}
