package com.adam.task.userDetail.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adam.task.R;
import com.adam.task.databinding.SelectedUserBinding;
import com.adam.task.entity.Address;
import com.adam.task.entity.Users;

import java.util.ArrayList;
import java.util.List;

public class SelectedUserAdapter extends RecyclerView.Adapter<SelectedUserAdapter.SelectedUserViewHolder> {

    private List<Users> data;
    private OnClickListener listener;

    public SelectedUserAdapter() {
        data = new ArrayList<>();
    }

    public void setData(List<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public SelectedUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SelectedUserBinding vie = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.selected_user, parent, false);
        return new SelectedUserViewHolder(vie);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedUserViewHolder holder, int position) {
        Address add = data.get(position).getAddress();
        holder.binding.text.setText(holder.itemView.getContext().getString(R.string.address_lbl,
                add.getSuite(), add.getStreet(), add.getCity(), add.getZipcode()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    interface OnClickListener {
        void onClickUser(int position);
    }

    class SelectedUserViewHolder extends RecyclerView.ViewHolder {

        private SelectedUserBinding binding;

        SelectedUserViewHolder(SelectedUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.showMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClickUser(getAdapterPosition());
                }
            });
        }
    }
}
