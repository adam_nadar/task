package com.adam.task.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.adam.task.ListHome.view.ListAct;
import com.adam.task.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ListAct.start(this);
        finish();
    }
}
