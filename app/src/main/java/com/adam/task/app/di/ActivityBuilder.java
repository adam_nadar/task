package com.adam.task.app.di;

import com.adam.task.ListHome.module.ListModule;
import com.adam.task.ListHome.view.ListAct;
import com.adam.task.UserCordinates.UserCordinateModule;
import com.adam.task.UserCordinates.UserCordinteAct;
import com.adam.task.userDetail.module.UserDetailModule;
import com.adam.task.userDetail.view.UserDetailActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = ListModule.class)
    abstract ListAct contributeListAct();

    @ContributesAndroidInjector(modules = UserDetailModule.class)
    abstract UserDetailActivity contributeUserDetailActivity();

    @ContributesAndroidInjector(modules = UserCordinateModule.class)
    abstract UserCordinteAct contributeUserCordinateActivity();
}
