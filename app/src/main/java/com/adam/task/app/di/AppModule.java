package com.adam.task.app.di;

import android.app.Application;
import android.content.Context;

import com.adam.task.BuildConfig;
import com.adam.task.ListHome.contract.ListActContract;
import com.adam.task.ListHome.model.ListActModel;
import com.adam.task.entity.Users;
import com.adam.task.util.network.TaskService;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context providesContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    TaskService providesApiService(Retrofit retrofit) {
        return retrofit.create(TaskService.class);
    }

    @Provides
    @Singleton
    Call<List<Users>> providesApiServiceCall(TaskService service) {
        return service.getUsers();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(GsonConverterFactory gsonConverterFactory,
                              OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG)
            client.addNetworkInterceptor(new StethoInterceptor());

        return client.build();
    }

    @Provides
    @Singleton
    Cache providesOkhttpCache(Context context) {
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        return new Cache(context.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Singleton
    @Provides
    ListActContract.Model providesListActModel(Call<List<Users>> call) {
        return new ListActModel(call);
    }
}
